# Nike Interview Backend Code Challenge [Java]

## Prerequisites

This project uses Maven & Spring Boot (Web Starter Pack). So you will need to install `java (> 1.8)` & `maven` on your machine first.
For more information, you can visit the [Spring Boot docs](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html#getting-started.introducing-spring-boot)

1. You can run `mvn dependency:tree` to list/install the maven dependencies used in this project.
2. To run the server (on port 8081), use this command: `mvn spring-boot:run`

## Background

We have built a light project (frontend and backend) to help customers in finding out the right 
time to buy a bunch of Nike shoe models. 

Each shoe model has its Minimum & Maximum Price Range. A frontend application shows 
the real-time varying prices (fetched from backend) of all a group of Nike shoe models.

If the <u>current price of a shoe model is less than its minimum price</u>, the frontend app shows: 
_Best time to buy!_. Similarly, if the <u>current shoe price is greater than its maximum price</u>, the 
app shows: _Can wait for discount_. Furthermore, if the current shoe price is between its 
minimum & maximum price range, the app shows: _Moderate state, can buy now!_

The minimum & max price ranges of the shoe models are:

| Shoe ID    | Shoe Model           | Minimum Price (USD)   | Maximum Price (USD)   |
| ---------- | -------------------- | --------------------- | --------------------- |
| 1          | Nike Air Max 95 SE   | 120                   | 150                   |
| 2          | Nike Air Max 97 SE   | 5                     | 150                   |
| 3          | Nike Air Max Pre-Day | 120                   | 160                   |
| 4          | Nike Air Max 270     | 100                   | 130                   |
| 5          | Nike Renew Ride 3    | 180                   | 200                   |
| 6          | Nike Air Max 90      | 120                   | 150                   |

## Tasks
1. Due to the time constraints, there were no Test Cases added on the frontend & 
backend projects initially. You’re required to add tests to the frontend & backend 
projects. Please feel free to think around multiple use-cases, that can be tested in the 
apps. Also, please feel free to refactor/restructure the code, if required.

2. Currently, the backend API simply returns a random price of each shoe model. You 
need to modify or create a new API, that adds a common flat discount of 40% to all 
shoe models and returns both the original & discounted prices.

Due to the time constraint, you might need to prioritize your tasks according to their 
criticality. Please mention in the README, regarding the reasoning behind your 
prioritization.

## APIs

### Get original price (randomly fetched) for a supplied shoe id

Endpoint

```text
GET /api/shoe-price/{id}
```

Parameters

| Parameter      | Description                              |
| -------------- | ---------------------------------------- |
| `id`           | One of the shoe id listed above          |

Retrieving readings using CURL

```console
$ curl "http://localhost:8081/api/shoe-price/1"
```

Example output

```json
{
    "code":200
    "msg":NULL
    "data":{
        "originalPrice":155
        "discountedPrice":62
    }
}
```

## Others

### Git Repository
- [nike-shoes-backend-java](https://gitlab.com/hiring_nike_china/nike-shoes-backend-java)
- [nike-shoes-frontend-vue-app](https://gitlab.com/hiring_nike_china/nike-shoes-frontend-vue-app)

### Suppliment of README
- Description of your changes & code improvements.
- If you had more time, what would you add further.
- What were your doubts, and what were your assumptions for the projects?
- Any other notes, that are relevant to your task.