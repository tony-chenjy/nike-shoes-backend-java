package com.nikeshoes.controller;

import java.util.Optional;

import com.nikeshoes.domain.ShoePrice;
import com.nikeshoes.exception.ResponseType;
import com.nikeshoes.service.ShoePriceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShoePriceController {

    @Autowired
    private ShoePriceService shoePriceService;

    @GetMapping("/api/shoe-price/{id}")
    @ResponseBody
    public ResponseType getShoePrice(@PathVariable String id) {
        Optional<ShoePrice> shoePrice = this.shoePriceService.getPriceByShoeId(id);

        if (!shoePrice.isPresent()) {
            return ResponseType.error(HttpStatus.NOT_FOUND);
        }

        return ResponseType.success(shoePrice.get());
    }
    
}
