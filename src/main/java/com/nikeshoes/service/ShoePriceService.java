package com.nikeshoes.service;

import java.util.Optional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.nikeshoes.domain.ShoePrice;
import com.nikeshoes.util.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ShoePriceService {

    @Autowired
    private RestTemplate restTemplate;

    public Optional<ShoePrice> getPriceByShoeId(String id) {
        String url = "https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id="+id;
        String response = this.restTemplate.getForObject(url, String.class);
        if (response == null) {
            return Optional.empty();
        }
        
        JSONObject jsonObject = JSON.parseObject(response);
        Double shoePrice = jsonObject.getDouble(Constants.KEY_SHOE_PRICE);
        if (shoePrice == null) {
            return Optional.empty();
        }
        
        return Optional.ofNullable(new ShoePrice(shoePrice));
    }
    
}
