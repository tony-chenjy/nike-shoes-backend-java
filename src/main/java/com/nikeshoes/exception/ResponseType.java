package com.nikeshoes.exception;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class ResponseType {

    private int code;
    private String msg;
    private Object data;

    private ResponseType(int code) {
        this.code = code;
    }

    private ResponseType(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private ResponseType(int code, Object data) {
        this.code = code;
        this.data = data;
    }
    
    public static ResponseType success(Object data) {
        return new ResponseType(HttpStatus.OK.value(), data);
    }
    
    public static ResponseType error(HttpStatus code) {
        return new ResponseType(code.value());
    }
    
    public static ResponseType error() {
        return new ResponseType(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
    
    public static ResponseType error(String msg) {
        return new ResponseType(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg);
    }
}
