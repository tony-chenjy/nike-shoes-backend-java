package com.nikeshoes.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    
    @ExceptionHandler(Exception.class)
    public ResponseType exceptionHandler(Exception ex) {
        return ResponseType.error(ex.getMessage());
    }

}
