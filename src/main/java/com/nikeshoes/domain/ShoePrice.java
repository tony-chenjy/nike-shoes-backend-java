package com.nikeshoes.domain;

import lombok.Data;

@Data
public class ShoePrice {

    public static final double DISCOUNT = 0.4;

    private Double originalPrice;
    private Double discountedPrice;

    public ShoePrice(Double originalPrice) {
        this.originalPrice = originalPrice;
        this.discountedPrice = originalPrice * DISCOUNT;
    }
    
}
