package com.nikeshoes.controller;

import java.net.URI;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nikeshoes.domain.ShoePrice;
import com.nikeshoes.exception.ResponseType;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
public class ShoePriceControllerTest {
    
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private RestTemplate restTemplate;
    private MockRestServiceServer mockServer;
    private ObjectMapper mapper;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mapper = new ObjectMapper();
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testGetShoePriceOK() throws Exception {
        ResponseType expected = ResponseType.success(new ShoePrice(147d));

        mockServer.expect(
            ExpectedCount.once(), 
            MockRestRequestMatchers.requestTo(new URI("https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id=1"))
        ).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(
            MockRestResponseCreators
            .withStatus(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(mapper.writeValueAsString(JSONObject.parse("{\"shoePrice\": 147}")))
        );

        mockMvc.perform(MockMvcRequestBuilders.get("/api/shoe-price/1"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().json(JSONObject.toJSONString(expected)))
        .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testRestTemplateResponseNull() throws Exception {
        ResponseType expected = ResponseType.error(HttpStatus.NOT_FOUND);

        mockServer.expect(
            ExpectedCount.once(), 
            MockRestRequestMatchers.requestTo(new URI("https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id=1"))
        ).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/shoe-price/1"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().json(JSONObject.toJSONString(expected)))
        .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testRestTemplateResponseMismatch() throws Exception {
        ResponseType expected = ResponseType.error(HttpStatus.NOT_FOUND);

        mockServer.expect(
            ExpectedCount.once(), 
            MockRestRequestMatchers.requestTo(new URI("https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id=1"))
        ).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(
            MockRestResponseCreators
            .withStatus(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(mapper.writeValueAsString(JSONObject.parse("{\"misMatchShoePrice\": 147}")))
        );

        mockMvc.perform(MockMvcRequestBuilders.get("/api/shoe-price/1"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().json(JSONObject.toJSONString(expected)))
        .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void testRestTemplateRequestError() throws Exception {
        ResponseType expected = ResponseType.error(HttpStatus.INTERNAL_SERVER_ERROR);

        mockServer.expect(
            ExpectedCount.once(), 
            MockRestRequestMatchers.requestTo(new URI("https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id=1"))
        ).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(MockRestResponseCreators.withStatus(HttpStatus.NOT_FOUND));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/shoe-price/1"))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(MockMvcResultMatchers.content().json(JSONObject.toJSONString(expected)))
        .andDo(MockMvcResultHandlers.print());
    }

}