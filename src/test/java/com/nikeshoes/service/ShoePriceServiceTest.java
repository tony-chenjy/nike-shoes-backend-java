package com.nikeshoes.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Optional;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nikeshoes.domain.ShoePrice;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class ShoePriceServiceTest {

    @Autowired
    private ShoePriceService shoePriceService;
    @Autowired
    private RestTemplate restTemplate;
    private MockRestServiceServer mockServer;
    private ObjectMapper mapper;

    @BeforeEach
    void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        mapper = new ObjectMapper();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void testGetPriceByShoeIdOK() throws Exception {
        Optional<ShoePrice> expected = Optional.ofNullable(new ShoePrice(147d));

        mockServer.expect(
            ExpectedCount.once(), 
            MockRestRequestMatchers.requestTo(new URI("https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id=1"))
        ).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(
            MockRestResponseCreators
            .withStatus(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(mapper.writeValueAsString(JSONObject.parse("{\"shoePrice\": 147}")))
        );
        Optional<ShoePrice> actual = shoePriceService.getPriceByShoeId("1");

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void testRestTemplateResponseNull() throws Exception {
        Optional<ShoePrice> expected = Optional.empty();

        mockServer.expect(
            ExpectedCount.once(), 
            MockRestRequestMatchers.requestTo(new URI("https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id=1"))
        ).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK));
        Optional<ShoePrice> actual = shoePriceService.getPriceByShoeId("1");

        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void testRestTemplateResponseMismatch() throws Exception {
        Optional<ShoePrice> expected = Optional.empty();

        mockServer.expect(
            ExpectedCount.once(), 
            MockRestRequestMatchers.requestTo(new URI("https://bi8cxjuyll.execute-api.us-west-2.amazonaws.com/prices/shoes?id=1"))
        ).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(
            MockRestResponseCreators
            .withStatus(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON)
            .body(mapper.writeValueAsString(JSONObject.parse("{\"misMatchShoePrice\": 147}")))
        );
        Optional<ShoePrice> actual = shoePriceService.getPriceByShoeId("1");

        assertThat(actual).isEqualTo(expected);
    }

}